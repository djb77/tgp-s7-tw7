#!/sbin/sh
# --------------------------------
# TGP AROMA INSTALLER v9.34
# tgp.sh portion
#
# Created by @djb77 from XDA
# Credit also goes to @lyapota
# @farovitus, @Morogoku, @dwander,
# @Chainfire and @osm0sis for
# snippets of code.
#
# DO NOT USE ANY PORTION OF THIS
# CODE WITHOUT MY PERMISSION!!
# --------------------------------

# Read option number from updater-script
OPTION=$1

# Variables
TGPTEMP=/tmp/tgptemp
AROMA=/tmp/aroma
CONFIG=/data/media/0/TGP/config
KERNELPATH=/tmp/tgptemp/kernels
RECOVERYPATH=/tmp/tgptemp/recovery
MODEMPATH=/tmp/tgptemp/modem
BUILDPROP=/system/build.prop
remove_list="init.services.rc init.PRIME-Kernel.rc init.spectrum.sh init.spectrum.rc init.primal.rc init.noto.rc kernelinit.sh wakelock.sh super.sh cortexbrain-tune.sh spectrum.sh kernelinit.sh spa init_d.sh initd.sh moro-init.sh sysinit.sh"

if [ $OPTION == "setup" ]; then
	# Set Permissions
	chmod 755 $AROMA/tar
	chmod 755 $AROMA/zip
	chmod 755 $AROMA/tgp.sh
	# Variant Check
	getprop ro.boot.bootloader >> /tmp/variant_model
	if grep -q G930F /tmp/variant_model;
	then
		echo "install=1" > $AROMA/g930x.prop
		echo "install=1" > $AROMA/g930f.prop
	fi
	if grep -q G930FD /tmp/variant_model;
	then
		echo "install=1" > $AROMA/g930x.prop
		echo "install=1" > $AROMA/g930f.prop
	fi
	if grep -q G930K /tmp/variant_model;
	then
		echo "install=1" > $AROMA/g930x.prop
	fi
	if grep -q G930L /tmp/variant_model;
	then
		echo "install=1" > $AROMA/g930x.prop
	fi
	if grep -q G930S /tmp/variant_model;
	then
		echo "install=1" > $AROMA/g930x.prop
	fi
	if grep -q G930W8 /tmp/variant_model;
	then
		echo "install=1" > $AROMA/g930x.prop
		echo "install=1" > $AROMA/g93xw8.prop
		echo "install=1" > $AROMA/g930w8.prop
	fi
	if grep -q G935F /tmp/variant_model;
	then
		echo "install=1" > $AROMA/g935f.prop
		echo "install=1" > $AROMA/g935x.prop
	fi
	if grep -q G935FD /tmp/variant_model;
	then
		echo "install=1" > $AROMA/g935f.prop
		echo "install=1" > $AROMA/g935x.prop
	fi
	if grep -q G935W8 /tmp/variant_model;
	then
		echo "install=1" > $AROMA/g93xw8.prop
		echo "install=1" > $AROMA/g935x.prop
		echo "install=1" > $AROMA/g935w8.prop
	fi
	# Config Check
	mount /dev/block/platform/155a0000.ufs/by-name/USERDATA /data
	# If config backup is present, alert installer
	if [ -e $CONFIG/tgp-backup.prop ]; then
		echo "install=1" > /tmp/aroma/backup.prop
	fi
	exit 10
fi

if [ $OPTION == "setup_extract" ]; then
	# Extract Files
	if grep -q install=1 $AROMA/kernelflash.prop; then
		cd $KERNELPATH
		$AROMA/tar -Jxf kernels.tar.xz
	fi
	if grep -q install=1 $AROMA/recovery.prop; then
		cd $RECOVERYPATH
		$AROMA/tar -Jxf recovery.tar.xz
	fi
	if grep -q install=1 $AROMA/modem.prop; then
		cd $MODEMPATH
		$AROMA/tar -Jxf modem.tar.xz
	fi
	exit 10
fi

if [ $OPTION == "config_backup" ]; then
	## Backup Config
	# Check if TGP folder exists on Internal Memory, if not, it is created
	if [ ! -d /data/media/0/TGP ]; then
	  mkdir /data/media/0/TGP
	  chmod 777 /data/media/0/TGP
	fi
	# Check if config folder exists, if it does, delete it 
	if [ -d $CONFIG-backup ]; then
	  rm -rf $CONFIG-backup
	fi
	# Check if config folder exists, if it does, ranme to backup
	if [ -d $CONFIG ]; then
	  mv -f $CONFIG $CONFIG-backup
	fi
	# Check if config folder exists, if not, it is created
	if [ ! -d $CONFIG ]; then
	  mkdir $CONFIG
	  chmod 777 $CONFIG
	fi
	# Copy files from $AROMA to backup location
	cp -f $AROMA/* $CONFIG
	# Delete any files from backup that are not .prop files
	find $CONFIG -type f ! -iname "*.prop" -delete
	# Remove unwanted .prop files from the backup
	cd $CONFIG
	for delete_prop in *.prop 
	do
	  if grep "item" "$delete_prop"; then
	    rm -f $delete_prop
	  fi
	  if grep "install=0" "$delete_prop"; then
	    rm -f $delete_prop
	  fi 
	done
	exit 10
fi

if [ $OPTION == "config_restore" ]; then
	## Restore Config
	# Copy backed up config files to $AROMA
	cp -f $CONFIG/* $AROMA
	exit 10
fi

if [ $OPTION == "backup_efs" ]; then	
	## Backup EFS
	mount /dev/block/platform/155a0000.ufs/by-name/EFS /efs
	if [ ! -d /data/media/0/TGP ];then
	mkdir /data/media/0/TGP
	chmod -R 777 /data/media/0/TGP
	fi
	if [ -e /data/media/0/TGP/efs.img.bak ];then
	rm -f /data/media/0/TGP/efs.img.bak 
	fi
	if [ -e /data/media/0/TGP/efs.img ];then
	cp -f /data/media/0/TGP/efs.img /data/media/0/TGP/efs.img.bak
	rm -f /data/media/0/TGP/efs.img
	fi
	dd if=/dev/block/sda3 of=/data/media/0/TGP/efs.img bs=4096
	exit 10
fi

if [ $OPTION == "wipe_magisk" ]; then
	## Wipe SuperSU / Magisk
	rm -rf /system/app/magisk
	rm -rf /cache/*magisk* /cache/unblock /data/*magisk* /data/cache/*magisk* /data/property/*magisk* \
	       /data/Magisk.apk /data/busybox /data/custom_ramdisk_patch.sh /data/app/com.topjohnwu.magisk* \
	       /data/user*/*/magisk.db /data/user*/*/com.topjohnwu.magisk /data/user*/*/.tmp.magisk.config \
	       /data/adb/*magisk* 2>/dev/null
	rm -rf /data/SuperSU.apk /data/su.img /data/stock_boot*.gz /data/supersu /supersu /data/adb/su/suhide
	exit 10
fi

if [ $OPTION == "wipe" ]; then
	## Full Wipe
	find /data -maxdepth 1 -type d ! -path "/data" ! -path "/data/media" | xargs rm -rf
	exit 10
fi

if [ $OPTION == "variant_fix" ]; then
	## Variant Fix
	getprop ro.boot.bootloader >> /tmp/variant_model
	if [ -e /system/CSCVersion.txt ]; then
		if grep -q G935 /tmp/variant_model;
		then
		sed -i -- 's/G930/G935/g' /system/CSCVersion.txt
		sed -i -- 's/G930/G935/g' /system/SW_Configuration.xml
		fi
		if grep -q G930 /tmp/variant_model;
		then
		sed -i -- 's/G935/G930/g' /system/CSCVersion.txt
		sed -i -- 's/G935/G930/g' /system/SW_Configuration.xml
		fi
	fi
	if grep -q G930F /tmp/variant_model;
	then
		sed -i -- 's/G935F/G930F/g' $BUILDPROP
		sed -i -- 's/hero2lte/herolte/g' $BUILDPROP
	fi
	if grep -q G930FD /tmp/variant_model;
	then
		sed -i -- 's/G935F/G930FD/g' $BUILDPROP
		sed -i -- 's/hero2lte/herolte/g' $BUILDPROP
	fi
	if grep -q G930K /tmp/variant_model;
	then
		sed -i -- 's/G935F/G930K/g' $BUILDPROP
		sed -i -- 's/hero2lte/herolte/g' $BUILDPROP
	fi
	if grep -q G930L /tmp/variant_model;
	then
		sed -i -- 's/G935F/G930L/g' $BUILDPROP
		sed -i -- 's/hero2lte/herolte/g' $BUILDPROP
	fi
	if grep -q G930S /tmp/variant_model;
	then
		sed -i -- 's/G935F/G930S/g' $BUILDPROP
		sed -i -- 's/hero2lte/herolte/g' $BUILDPROP
	fi
	if grep -q G930W8 /tmp/variant_model;
	then
		sed -i -- 's/G935F/G930W8/g' $BUILDPROP
		sed -i -- 's/hero2lte/herolte/g' $BUILDPROP
	fi
	if grep -q G935FD /tmp/variant_model;
	then
		sed -i -- 's/G935F/G935FD/g' $BUILDPROP
	fi
	if grep -q G935K /tmp/variant_model;
	then
		sed -i -- 's/G935F/G935K/g' $BUILDPROP
	fi
	if grep -q G935L /tmp/variant_model;
	then
		sed -i -- 's/G935F/G935L/g' $BUILDPROP
	fi
	if grep -q G935S /tmp/variant_model;
	then
		sed -i -- 's/G935F/G935S/g' $BUILDPROP
	fi
	if grep -q G935W8 /tmp/variant_model;
	then
		sed -i -- 's/G935F/G935W8/g' $BUILDPROP
	fi
	sed -i -- 's/ro.product.device=herolte/ro.product.device=hero2lte/g' $BUILDPROP
	sed -i -- 's/ro.build.product=herolte/ro.build.product=hero2lte/g' $BUILDPROP
	exit 10
fi

if [ $OPTION == "buildprop_tweaks" ]; then
	## build.prop Fixes
	sed -i -- 's/ro.securestorage.support=true/ro.securestorage.support=false/g' $BUILDPROP
	sed -i -- 's/ro.config.dmverity=true/ro.config.dmverity=false/g' $BUILDPROP
	echo "# Screen mirror fix" >> $BUILDPROP
	echo "wlan.wfd.hdcp=disable" >> $BUILDPROP
	if grep -q install=1 $AROMA/tweaks-1.prop; then
		echo "# General Tweaks" >> $BUILDPROP
		echo "fingerprint.unlock=1" >> $BUILDPROP
		echo "wlan.wfd.hdcp=disable" >> $BUILDPROP
		echo "ro.media.enc.jpeg.quality=100" >> $BUILDPROP
		echo "persist.cne.feature=0" >> $BUILDPROP
		echo "ro.telephony.call_ring.delay=0" >> $BUILDPROP
		echo "ro.lge.proximity.delay=15" >> $BUILDPROP
		echo "mot.proximity.delay=15" >> $BUILDPROP
		echo "pm.sleep_mode=1" >> $BUILDPROP
		echo "debug.performance.tuning=1" >> $BUILDPROP
		echo "ro.config.hw_fast_dormancy=1" >> $BUILDPROP
		echo "ro.config.vc_call_steps=20" >> $BUILDPROP
		echo "ro.ril.enable.amr.wideband=1" >> $BUILDPROP
		echo "ro.config.disable.hw_accel=false" >> $BUILDPROP
		echo "ro.fb.mode=1" >> $BUILDPROP
		echo "ro.sf.compbypass.enable=0" >> $BUILDPROP
		echo "persist.sys.ui.hw=1" >> $BUILDPROP
		echo "power.saving.mode=1" >> $BUILDPROP
		echo "ro.config.hw_quickpoweron=true" >> $BUILDPROP
		echo "ro.config.hw_power_saving=true" >> $BUILDPROP
		echo "ro.ril.disable.power.collapse=1" >> $BUILDPROP
		echo "wifi.supplicant_scan_interval=180" >> $BUILDPROP
	fi
	if grep -q install=1 $AROMA/tweaks-2.prop; then
		echo "# Animation Tweaks" >> $BUILDPROP
		echo "persist.sys.shutdown.mode=hibernate" >> $BUILDPROP
		echo "boot.fps=25" >> $BUILDPROP
		echo "shutdown.fps=25" >> $BUILDPROP
	fi
	if grep -q install=1 $AROMA/tweaks-3.prop; then
		echo "# Network Tweaks" >> $BUILDPROP
		echo "net.dns1=8.8.8.8" >> $BUILDPROP
		echo "net.dns2=8.8.4.4" >> $BUILDPROP
		echo "net.eth0.dns1=8.8.8.8" >> $BUILDPROP
		echo "net.eth0.dns2=8.8.4.4" >> $BUILDPROP
		echo "net.gprs.dns1=8.8.8.8" >> $BUILDPROP
		echo "net.gprs.dns2=8.8.4.4" >> $BUILDPROP
		echo "net.ipv4.ip_no_pmtu_disc=0" >> $BUILDPROP
		echo "net.ipv4.route.flush=1" >> $BUILDPROP
		echo "net.ipv4.tcp_ecn=0" >> $BUILDPROP
		echo "net.ipv4.tcp_fack=1" >> $BUILDPROP
		echo "net.ipv4.tcp_mem=187000" >> $BUILDPROP
		echo "net.ipv4.tcp_moderate_rcvbuf=1" >> $BUILDPROP
		echo "net.ipv4.tcp_no_metrics_save=1" >> $BUILDPROP
		echo "net.ipv4.tcp_rfc1337=1" >> $BUILDPROP
		echo "net.ipv4.tcp_rmem=4096" >> $BUILDPROP
		echo "net.ipv4.tcp_sack=1" >> $BUILDPROP
		echo "net.ipv4.tcp_timestamps=1" >> $BUILDPROP
		echo "net.ipv4.tcp_window_scaling=1" >> $BUILDPROP
		echo "net.ipv4.tcp_wmem=4096" >> $BUILDPROP
		echo "net.ppp0.dns1=8.8.8.8" >> $BUILDPROP
		echo "net.ppp0.dns2=8.8.4.4" >> $BUILDPROP
		echo "net.rmnet0.dns1=8.8.8.8" >> $BUILDPROP
		echo "net.rmnet0.dns2=8.8.4.4" >> $BUILDPROP
		echo "net.tcp.buffersize.default=4096,87380,256960,4096,16384,256960" >> $BUILDPROP
		echo "net.tcp.buffersize.edge=4096,87380,256960,4096,16384,256960" >> $BUILDPROP
		echo "net.tcp.buffersize.evdo_b=6144,87380,1048576,6144," >> $BUILDPROP
		echo "net.tcp.buffersize.gprs=4096,87380,256960,4096,16384,256960" >> $BUILDPROP
		echo "net.tcp.buffersize.hsdpa=6144,87380,1048576,6144,8" >> $BUILDPROP
		echo "net.tcp.buffersize.hspa=6144,87380,524288,6144,163" >> $BUILDPROP
		echo "net.tcp.buffersize.lte=524288,1048576,2097152,5242" >> $BUILDPROP
		echo "net.tcp.buffersize.umts=4096,87380,256960,4096,16384,256960" >> $BUILDPROP
		echo "net.tcp.buffersize.wifi=4096,87380,256960,4096,16384,256960" >> $BUILDPROP
		echo "net.wlan0.dns1=8.8.8.8" >> $BUILDPROP
		echo "net.wlan0.dns2=8.8.4.4" >> $BUILDPROP
		echo "# Signal Tweaks" >> $BUILDPROP
		echo "persist.cust.tel.eons=1" >> $BUILDPROP
		echo "ro.ril.enable.3g.prefix=1" >> $BUILDPROP
		echo "persist.telephony.support.ipv4=1" >> $BUILDPROP
		echo "persist.telephony.support.ipv6=1" >> $BUILDPROP
		echo "ro.ril.enable.dtm=1" >> $BUILDPROP
		echo "ro.ril.gprsclass=10" >> $BUILDPROP
		echo "ro.ril.hep=1" >> $BUILDPROP
		echo "ro.ril.hsdpa.category=10" >> $BUILDPROP
		echo "ro.ril.hsupa.category=5" >> $BUILDPROP
		echo "ro.ril.hsxpa=2" >> $BUILDPROP
		echo "ro.telephony.call_ring.delay=0" >> $BUILDPROP
	fi
	if grep -q install=1 $AROMA/tweaks-4.prop; then
		echo "# Multiuser Tweaks" >> $BUILDPROP
		echo "fw.max_users=10" >> $BUILDPROP
		echo "fw.show_multiuserui=1" >> $BUILDPROP
		echo "fw.show_hidden_users=1" >> $BUILDPROP
		echo "fw.power_user_switcher=1" >> $BUILDPROP
	fi
	if grep -q install=1 $AROMA/samsung-10.prop; then
		sed -i -- 's/ro.config.tima=1/ro.config.tima=0/g' $BUILDPROP
	fi
	exit 10
fi

if [ $OPTION == "apk_patch" ]; then
	## APK Patching Scripts
	# Patching Script
	FUNC_PATCH()
	{
	mkdir /tmp/work
	cp -f $APKLocation/$APKName/$APKName.apk /tmp/apk_patch/$APKName.zip
	cd $APKPatch
	$AROMA/zip -r -0 /tmp/apk_patch/$APKName.zip *
	rm -rf $APKLocation/$APKName/$APKName.apk
	cp -f /tmp/apk_patch/$APKName.zip $APKLocation/$APKName/$APKName.apk
	rm -rf /tmp/work
	}
	# Patching Script for framework-res
	FUNC_PATCH_FRAMEWORK()
	{
	mkdir /tmp/work
	cp -f $APKLocation/$APKName.apk /tmp/apk_patch/$APKName.zip
	cd $APKPatch
	$AROMA/zip -r -0 /tmp/apk_patch/$APKName.zip *
	rm -rf $APKLocation/$APKName/$APKName.apk
	cp -f /tmp/apk_patch/$APKName.zip $APKLocation/$APKName.apk
	rm -rf /tmp/work
	}
	# Patch framework-res
	APKLocation=/system/framework
	APKName=framework-res
	APKPatch=/tmp/apk_patch/framework-res
	[ -d /tmp/apk_patch/framework-res ] && FUNC_PATCH_FRAMEWORK
	# Patch samsung-framework-res
	APKLocation=/system/framework
	APKName=samsung-framework-res
	APKPatch=/tmp/apk_patch/samsung-framework-res
	[ -d /tmp/apk_patch/samsung-framework-res ] && FUNC_PATCH
	# Patch SamsungCamera6 
	APKLocation=/system/priv-app
	APKName=SamsungCamera6
	APKPatch=/tmp/apk_patch/SamsungCamera6
	[ -d /tmp/apk_patch/SamsungCamera6 ] && FUNC_PATCH
	# Patch SecMyFiles2017
	APKLocation=/system/priv-app 
	APKName=SecMyFiles2017
	APKPatch=/tmp/apk_patch/SecMyFiles2017
	[ -d /tmp/apk_patch/SecMyFiles2017 ] && FUNC_PATCH
	# Patch SecSettings2
	APKLocation=/system/priv-app
	APKName=SecSettings2
	APKPatch=/tmp/apk_patch/SecSettings2
	[ -d /tmp/apk_patch/SecSettings2 ] && FUNC_PATCH
	# Patch SecSetupWizard2015
	APKLocation=/system/priv-app
	APKName=SecSetupWizard2015
	APKPatch=/tmp/apk_patch/SecSetupWizard2015
	[ -d /tmp/apk_patch/SecSetupWizard2015 ] && FUNC_PATCH
	# Patch SystemUI
	APKLocation=/system/priv-app
	APKName=SystemUI
	APKPatch=/tmp/apk_patch/SystemUI
	[ -d /tmp/apk_patch/SystemUI ] && FUNC_PATCH
	exit 10
fi

if [ $OPTION == "splash_flash" ]; then
	## Custom Splash Screen
	cd /tmp/splash
	mkdir /tmp/splashtmp
	cd /tmp/splashtmp
	$AROMA/tar -xf /dev/block/platform/155a0000.ufs/by-name/PARAM
	cp /tmp/splash/logo.jpg .
	chown root:root *
	chmod 444 logo.jpg
	touch *
	$AROMA/tar -pcvf ../new.tar *
	cd ..
	cat new.tar > /dev/block/platform/155a0000.ufs/by-name/PARAM
	cd /
	rm -rf /tmp/splash
	sync
fi

if [ $OPTION == "supersu" ]; then
	## SuperSU Script
	rm -f /data/.supersu
	rm -f /cache/.supersu
	if [ -f "$AROMA/install.prop" ]; then
		INSTALL=`cat $AROMA/install.prop | grep "selected.0" | cut -f 2 -d '='`
		if [ "$INSTALL" = "2" ]; then
			# System
			echo "SYSTEMLESS=false">>/data/.supersu
		elif [ "$INSTALL" = "3" ]; then
			# Systemless Image
			echo "SYSTEMLESS=true">>/data/.supersu
			echo "BINDSBIN=false">>/data/.supersu
		elif [ "$INSTALL" = "4" ]; then
			# Systemless SBIN
			echo "SYSTEMLESS=true">>/data/.supersu
			echo "BINDSBIN=true">>/data/.supersu
		fi
	fi
	if [ -f "$AROMA/encrypt.prop" ]; then
		KEEPVERITY=`cat $AROMA/encrypt.prop | grep "selected.1" | cut -f 2 -d '='`
		if [ "$KEEPVERITY" = "2" ]; then
			# Remove
			echo "KEEPVERITY=false">>/data/.supersu
		elif [ "$KEEPVERITY" = "3" ]; then
			# Keep
			echo "KEEPVERITY=true">>/data/.supersu
		fi
		KEEPFORCEENCRYPT=`cat $AROMA/encrypt.prop | grep "selected.2" | cut -f 2 -d '='`
		if [ "$KEEPFORCEENCRYPT" = "2" ]; then
			# Remove
			echo "KEEPFORCEENCRYPT=false">>/data/.supersu
		elif [ "$KEEPFORCEENCRYPT" = "3" ]; then
			# Keep
			echo "KEEPFORCEENCRYPT=true">>/data/.supersu
		fi
		REMOVEENCRYPTABLE=`cat $AROMA/encrypt.prop | grep "selected.3" | cut -f 2 -d '='`
		if [ "$REMOVEENCRYPTABLE" = "2" ]; then
			# Remove
			echo "REMOVEENCRYPTABLE=true">>/data/.supersu
		elif [ "$REMOVEENCRYPTABLE" = "3" ]; then
			# Keep
			echo "REMOVEENCRYPTABLE=false">>/data/.supersu
		fi
	fi
	if [ -f "$AROMA/misc.prop" ]; then
		FRP=`cat $AROMA/misc.prop | grep "selected.1" | cut -f 2 -d '='`
		if [ "$FRP" = "2" ]; then
			# Enable
			echo "FRP=true">>/data/.supersu
		elif [ "$FRP" = "3" ]; then
			# Disable
			echo "FRP=false">>/data/.supersu
		fi
		BINDSYSTEMXBIN=`cat $AROMA/misc.prop | grep "selected.2" | cut -f 2 -d '='`
		if [ "$BINDSYSTEMXBIN" = "2" ]; then
			# Enable
			echo "BINDSYSTEMXBIN=true">>/data/.supersu
		elif [ "$BINDSYSTEMXBIN" = "3" ]; then
			# Disable
			echo "BINDSYSTEMXBIN=false">>/data/.supersu
		fi
		PERMISSIVE=`cat $AROMA/misc.prop | grep "selected.3" | cut -f 2 -d '='`
		if [ "$PERMISSIVE" = "2" ]; then
			# Enforcing
			echo "PERMISSIVE=false">>/data/.supersu
		elif [ "$PERMISSIVE" = "3" ]; then
			# Permissive
			echo "PERMISSIVE=true">>/data/.supersu
		fi
	fi
fi

if [ $OPTION == "kernel_flash" ]; then
	# Clean up old kernels (@dwander)
	for i in $remove_list; do
		if test -f $i; then
			rm $i
			rm sbin/$i
			sed -i "/$i/d" init.rc 
			sed -i "/$i/d" init.samsungexynos8890.rc 
		fi
		if test -f sbin/$i; then
			rm sbin/$i
			sed -i "/sbin\/$i/d" init.rc 
			sed -i "/sbin\/$i/d" init.samsungexynos8890.rc 
		fi
	done
	for i in $(ls ./res); do
		test $i != "images" && rm -R ./res/$i
	done
	[ -f /system/bin/uci ] && rm -f /system/bin/uci
	[ -f /system/xbin/uci ] && rm -f /system/xbin/uci
	cd $KERNELPATH
	if grep -q install=1 $AROMA/kernel.prop; then
		# Flash TGPKernel
		cat tgp.img > /dev/block/platform/155a0000.ufs/by-name/BOOT
	fi
	if grep -q install=2 $AROMA/kernel.prop; then
		# Flash SuperKernel
		cat super.img > /dev/block/platform/155a0000.ufs/by-name/BOOT
	fi
fi

if [ $OPTION == "recovery_flash" ]; then
	cd $RECOVERYPATH
	if grep -q install=1 $AROMA/recovery.prop; then
		cat twrp.img > /dev/block/platform/155a0000.ufs/by-name/RECOVERY
	fi
fi

if [ $OPTION == "modem_flash" ]; then
	cd $MODEMPATH
	if grep -q install=1 $AROMA/modem.prop; then
		cat modem.bin > /dev/block/sda8
		cat modem_debug.bin > /dev/block/sda17
	fi
fi

