#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/155a0000.ufs/by-name/RECOVERY:45762560:339bbd4e515b3952fcf5d60634a8872e75b53f6e; then
  applypatch EMMC:/dev/block/platform/155a0000.ufs/by-name/BOOT:37595136:b9c00c76d518296f69dc95823138bf186bfa9888 EMMC:/dev/block/platform/155a0000.ufs/by-name/RECOVERY 339bbd4e515b3952fcf5d60634a8872e75b53f6e 45762560 b9c00c76d518296f69dc95823138bf186bfa9888:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
