#!/sbin/sh

sed -i '/deep_buffer {/,/}/d' /system/etc/audio_policy.conf

echo "" >> /system/build.prop
echo "#=====================================" >> /system/build.prop
echo "" >> /system/build.prop
echo "# XTREMEMusic by androidexpert35" >> /system/build.prop
echo "" >> /system/build.prop
echo "#=====================================" >> /system/build.prop
echo "audio.offload.buffer.size.kb=128" >> /system/build.prop
echo "audio.offload.pcm.24bit.enable=true" >> /system/build.prop
echo "audio.offload.pcm.32bit.enable=true" >> /system/build.prop
echo "audio.offload.pcm.64bit.enable=true" >> /system/build.prop
echo "htc.audio.swalt.enable=1" >> /system/build.prop
echo "htc.audio.swalt.mingain=14512" >> /system/build.prop
echo "htc.audio.alc.enable=1" >> /system/build.prop
echo "persist.audio.SupportHTCHWAEC=1" >> /system/build.prop
echo "af.resampler.quality=255" >> /system/build.prop
echo "persist.af.resampler.quality=255" >> /system/build.prop
echo "af.resample=52000" >> /system/build.prop
echo "persist.af.resample=52000" >> /system/build.prop
echo "ro.audio.samplerate=6144000" >> /system/build.prop
echo "persist.dev.pm.dyn_samplingrate=1" >> /system/build.prop
echo "ro.audio.pcm.samplerate=6144000" >> /system/build.prop
echo "AUDIODRIVER=alsa" >> /system/build.prop
echo "ro.sound.driver=alsa" >> /system/build.prop
echo "default.pcm.rate_converter=samplerate_best" >> /system/build.prop
echo "ro.sound.alsa=snd_pcm" >> /system/build.prop
echo "alsa.mixer.playback.master=Speaker" >> /system/build.prop
echo "alsa.mixer.capture.master=Mic" >> /system/build.prop
echo "alsa.mixer.playback.earpiece=Earpiece" >> /system/build.prop
echo "alsa.mixer.capture.earpiece=Mic" >> /system/build.prop
echo "alsa.mixer.playback.headset=Headset" >> /system/build.prop
echo "alsa.mixer.capture.headset=Mic" >> /system/build.prop
echo "alsa.mixer.playback.speaker=Speaker" >> /system/build.prop
echo "alsa.mixer.capture.speaker=Mic" >> /system/build.prop
echo "alsa.mixer.playback.bt.sco=BTHeadset" >> /system/build.prop
echo "alsa.mixer.capture.bt.sco=BTHeadset" >> /system/build.prop
echo "htc.audio.lpa.a2dp=0" >> /system/build.prop
echo "htc.audio.global.state=0" >> /system/build.prop
echo "htc.audio.global.profile=0" >> /system/build.prop
echo "htc.audio.q6.topology=0" >> /system/build.prop
echo "htc.audio.enable_dmic=1" >> /system/build.prop
echo "persist.htc.audio.pcm.samplerate=192000" >> /system/build.prop
echo "persist.htc.audio.pcm.channels=2" >> /system/build.prop
echo "persist.audio.fluence.mode=endfire" >> /system/build.prop
echo "persist.audio.vr.enable=false" >> /system/build.prop
echo "#Change under line to 'digital' if you have 2 mic" >> /system/build.prop
echo "persist.audio.handset.mic=analog" >> /system/build.prop
echo "htc.audio.swalt.enable=1" >> /system/build.prop
echo "htc.audio.swalt.mingain=14512" >> /system/build.prop
echo "htc.audio.alc.enable=1" >> /system/build.prop
echo "ro.ril.enable.amr.wideband=1" >> /system/build.prop
echo "ro.config.hw_dolby=true" >> /system/build.prop
echo "ro.config.hw_dts=true" >> /system/build.prop
echo "# Sony Audio & Resampling" >> /system/build.prop
echo "mpq.audio.decode=true" >> /system/build.prop
echo "sony.support.effect=0x1FF" >> /system/build.prop
echo "ro.semc.sound_effects_enabled=true" >> /system/build.prop
echo "ro.semc.xloud.supported=true" >> /system/build.prop
echo "persist.service.xloud.enable=1" >> /system/build.prop
echo "ro.semc.enhance.supported=true" >> /system/build.prop
echo "persist.service.enhance.enable=1" >> /system/build.prop
echo "ro.semc.clearaudio.supported=true" >> /system/build.prop
echo "persist.service.clearaudio.enable=1" >> /system/build.prop
echo "ro.sony.walkman.logger=1" >> /system/build.prop
echo "persist.service.walkman.enable=1" >> /system/build.prop
echo "ro.semc.clearphase.supported=true" >> /system/build.prop
echo "persist.service.clearphase.enable=1" >> /system/build.prop
echo "persist.service.enhance.enable=1" >> /system/build.prop
echo "ro.somc.sforce.supported=true" >> /system/build.prop
echo "persist.service.sforce.enable=1" >> /system/build.prop
echo "ro.somc.dseehx.supported=true" >> /system/build.prop
echo "persist.service.dseehx.enable=1" >> /system/build.prop
echo "com.sonymobile.dseehx_enabled=true" >> /system/build.prop
echo "com.sonyericsson.xloud_enabled=true" >> /system/build.prop
echo "ro.semc.xloud.default_setting=true" >> /system/build.prop
echo "com.sonymobile.clearphase_enabled=true" >> /system/build.prop
echo "ro.semc.cp.default_setting=true" >> /system/build.prop
echo "com.sonymobile.sforce_enabled=true" >> /system/build.prop
echo "ro.semc.sfs.default_setting=true" >> /system/build.prop
echo "# Harman Kardon" >> /system/build.prop
echo "support_harman=true" >> /system/build.prop
echo "support_boomsound_effect=true" >> /system/build.prop
echo "#=====================================" >> /system/build.prop
